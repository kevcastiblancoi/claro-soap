import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import  user  from "./components/assets/user.png";
import './App.css';



class LogIn extends Component {

  constructor() {
    super();
    this.state = {
      email: "",
      password: "" ,
      data:[]
    };
    this.Login=this.Login.bind(this)
  }
  Login() {
    console.log(this.props);
    
    this.props.history.replace('/header')
  }

  render() {
    return (
      <div className="login">
         <div className="form" >
            <Form>
              <img className="user-logo" src={user} alt='user' />
              <FormGroup>
                <Label style={{color: "white"}} for="exampleEmail">Correo</Label>
                <Input type="email" name="email" id="exampleEmail" placeholder="Correo" />
                <br></br>
                <Label style={{color: "white"}} for="exampleEmail">Contraseña</Label>
                <Input type="email" name="email" id="exampleEmail" placeholder="Contraseña" />
                
              </FormGroup>
              <Button className='log-btn' onClick={this.Login} color='secondary'>
                Iniciar Sesión
              </Button>
            </Form>
         </div>
      </div>
    );
  }
}

export default LogIn;
