import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import LogIn from './LogIn';
import header  from './components/batch/header';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Router>
    <div> 
     <Route path='/' exact  component={LogIn} />
     <Route  path="/header"  component={header} />
    </div>
</Router>
,
document.getElementById('root')
    );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
