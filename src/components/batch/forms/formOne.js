import React, { Component } from 'react';
import { Table, Input, Label  } from 'reactstrap';
import './formOne.css'

class formOne extends Component {

    state = {
        selectedOption: null,
      };
      handleChange = selectedOption => {
        this.setState({ selectedOption });
        console.log(`Option selected:`, selectedOption);
      };

     change (event) {
         console.log(event.target.value);
         
     } 
    render() {
        const checks = [
            { options: 'Area Limpia' },
            { options: 'Equipo Limpo' },
            { options: 'Metodología De Fabricación' },
            { options: 'Identificación De Materias primas dismensadas' },
            { options: 'Orden De Producción' },
            { options: 'Orden De Envase Y Empaque' },
            { options: 'Despeje De Linea' },
            { options: 'Orden DE Códificado' },
            { options: 'Control De Proceso' },
            { options: 'Reportes de Ánalisis De Producto' },
            { options: 'Reporte De Ánalisis Fisicoquimico' }
        ]
        return (
            <div className='format-container' >
                <Table bordered>
                    <thead>
                    <tr>
                        <th>Nombre del Producto</th>
                        <th>Número de lote</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                        <Label for="exampleSelect">JABÓN DE TOCADOR: </Label>
                        <Input type="select" name="selectMulti" id="exampleSelectMulti"  onChange={this.change} >
                            <option>SELECCIONAR</option>
                            <option>AVENA</option>
                            <option>CITRICO</option>
                            <option>FRESA</option>
                            <option>HOTELERO</option>
                            <option>LIMPIEZA PROFUNDA</option>
                        </Input>
                        </td>
                        <td className='td' >54654</td>
                    </tr>
                    </tbody>
                </Table>
                <Table bordered>
                   
                    <tbody>
                    <tr>
                        <td>
                            <Label for="exampleSelect">PRESENTACIÓN: </Label>
                            <Input type="text" name="password" id="examplePassword" placeholder="Presentación" />
                        </td>
                        <td>
                            <Label for="exampleSelect">UNIDADES FABRICADAS: </Label>
                            <Input type="text" name="password" id="examplePassword" placeholder="Unidades" />
                        </td>
                        <td>
                            <Label for="exampleSelect">PRESENTACIÓN: </Label>
                            <Input type="text" name="password" id="examplePassword" placeholder="Presentación" />
                        </td>
                        <td>
                            <Label for="exampleSelect">UNIDADES FABRICADAS: </Label>
                            <Input type="text" name="password" id="examplePassword" placeholder="Unidades" />
                        </td>
                    </tr>
                    
                    </tbody>
                </Table>
                <Table bordered>
                   
                   <tbody>
                        {
                            checks.map((items, index)=>{
                                return (
                                    <tr key={index}>
                                        <td> { items.options } </td>
                                        <td> <Input type="checkbox" id="checkbox2" /> </td>
                                    </tr>
                                    
                                    )
                            })
                        }
                   </tbody>
               </Table>
            </div>
        )
    }
}

export default formOne