import React, { Component } from 'react';
import  logoClaro  from './../assets/logo_claro.png'
import FormOne from './forms/formOne'
import './header.css'

class header extends Component {
  render() {
    return (
      <div>
          <div className="header">
            <div className="logo" >
            <img className="claro-logo" src={logoClaro} alt='user' />
            </div>
            <div className="title" >
              Sistema digital para control de calidad
            </div>
            <div className="user" >
              <p> Hernan Gonzalez </p>
              <p> Supervisor </p>
            </div>
         </div>
         <FormOne/>
      </div>
      
    );
  }
}

export default header;
